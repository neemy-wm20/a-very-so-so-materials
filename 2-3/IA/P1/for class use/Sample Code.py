# Example 1

# Code provided in Example 1 is obtained from the following link.
# https://www.pythonlikeyoumeanit.com/Module1_GettingStartedWithPython/Jupyter_Notebooks.html

import numpy as np
import matplotlib.pyplot as plt

# This tells Jupyter to embed matplotlib plots in the notebook
%matplotlib inline

def sinc(x):
    return np.sin(x) / x

def d_sinc(x):
    "derivative of sinc-function"
    return np.cos(x)/x - np.sin(x)/x**2

# Evaluate functions at 1000 points evenly spaced in [-15, 15]
x = np.linspace(-15, 15, 1000)
f = sinc(x)
df = d_sinc(x)

# Plot the sinc-function and its derivative
fig, ax = plt.subplots()
ax.plot(x, f, color="green", label=r"$sinc(x)$")
ax.plot(x, df, color="blue", ls="--", label=r"$\frac{d(sinc(x))}{dx}$")

ax.set_title("Example Notebook Plot")
ax.set_xlabel(r"$x$ [radians]")

ax.grid(True)
ax.legend();


# Example 2

# Code provided in Example 2 is obtained from the following link.
# https://www.simplilearn.com/tutorials/python-tutorial/data-visualization-in-python

import matplotlib.pyplot as plt
import seaborn as sns

%matplotlib inline

yield_apples = [0.895, 0.91, 0.919, 0.926, 0.929, 0.931]
plt.plot(yield_apples)


# Example 3
import matplotlib.pyplot as plt
import seaborn as sns
import random

%matplotlib inline

data_points = []

for index in range(10):
    data_points.append(random.random())
plt.plot(data_points)