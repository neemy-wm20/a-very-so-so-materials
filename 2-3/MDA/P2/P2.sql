DROP TABLE JobDone;
DROP TABLE Vehicle;
DROP TABLE Mechanic;
DROP TABLE Branch;

CREATE TABLE Branch(
branchno	number(4) NOT NULL,
street		varchar(20),
city		varchar(20),
postcode	number(5),
branchtelno	varchar(14),
rental		number(7,2),
PRIMARY KEY (branchno)
);

CREATE TABLE Vehicle(
vehicleno	varchar(7) NOT NULL,
vehicletype	varchar(20),
model		varchar(20),
makeyear	number(4),
colour		varchar(20),
PRIMARY KEY (vehicleno)
);

CREATE TABLE Mechanic(
mechanicid	number(4) NOT NULL,
mechanicname	varchar(20),
mecaddress	varchar(30),
qualification	varchar(30),
branchno	number(4),
salary		number(7,2) NOT NULL,
PRIMARY KEY (mechanicid),
FOREIGN KEY (branchno) REFERENCES Branch
);

CREATE TABLE JobDone(
jobno		number(7) NOT NULL,
jdate		date,
vehicleno	varchar(7),
mechanicid	number(4),
workdone	varchar(50),
charges		number(7,2),
PRIMARY KEY (jobno),
FOREIGN KEY (mechanicid) REFERENCES mechanic,
FOREIGN KEY (vehicleno) REFERENCES vehicle
);

INSERT INTO Branch
SELECT * FROM unknown.Branch;

INSERT INTO Vehicle
SELECT * FROM unknown.Vehicle;

INSERT INTO Mechanic
SELECT * FROM unknown.Mechanic;

INSERT INTO JobDone
SELECT * FROM unknown.JobDone;

COMMIT;

SELECT COUNT(*) AS NoOfBranch FROM Branch;
SELECT COUNT(*) AS NoOfVehicle FROM Vehicle;
SELECT COUNT(*) AS NoOfMechanic FROM Mechanic;
SELECT COUNT(*) AS NoOfJobDone FROM JobDone;