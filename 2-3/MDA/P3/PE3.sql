--Q1
CREATE OR REPLACE PROCEDURE prc_last_order (v_custNo IN NUMBER) IS
v_orderDate DATE;
v_duration NUMBER(4);
v_months NUMBER(4,1);
BEGIN
 SELECT MAX(OrderDate) INTO v_orderDate
 FROM Orders
 WHERE CustomerNumber = v_custNo;

 DBMS_OUTPUT.PUT_LINE('Customer ' || v_custNo || ' last made an order on ' || v_orderDate || '.');

 v_duration := ROUND(SYSDATE - v_orderDate, 0); 

 IF (v_duration < 30) THEN
  DBMS_OUTPUT.PUT_LINE('That was ' || v_duration || ' days ago.');
 ELSE
  v_months := MONTHS_BETWEEN (SYSDATE, v_orderDate);
  DBMS_OUTPUT.PUT_LINE('That was ' || v_months || ' months ago.');
 END IF;
END;
/

EXEC prc_last_order(382)

/*
Procedure created.

Customer 382 last made an order on 17-FEB-23.
That was 11 days ago.

PL/SQL procedure successfully completed.
*/

EXEC prc_last_order(166)

/*
Customer 166 last made an order on 23-JAN-23.
That was 1.1 months ago.

PL/SQL procedure successfully completed.
*/


--Q2
=======
create custom exception can give extra marks
=======
CREATE OR REPLACE PROCEDURE prc_discount_claim(v_OrderNo IN NUMBER, v_ProductCode IN VARCHAR) IS
	v_orderDate DATE;
	v_duration NUMBER(4);
	v_actualPrice NUMBER(7,2);
	v_buyPrice NUMBER(7,2);
	v_MinPrice NUMBER(7,2);
	v_discountedPrice NUMBER(7,2);

BEGIN
 SELECT MAX(OrderDate) INTO v_orderDate
 FROM Orders
 WHERE orderNumber = v_OrderNo AND (SYSDATE - orderDate) < 30;

 v_duration := ROUND(SYSDATE - v_orderDate, 0);

 DBMS_OUTPUT.PUT_LINE('Customer last made an order on ' || TO_CHAR(v_orderDate, 'DD/MM/YYYY') || ' .');
 DBMS_OUTPUT.PUT_LINE('That was ' || v_duration || ' days ago.');

 SELECT priceEach INTO v_actualPrice
 FROM orderDetails
 WHERE orderNumber = v_OrderNo AND productCode = v_ProductCode;

 SELECT buyPrice INTO v_buyPrice
 FROM Products
 WHERE productCode = v_ProductCode;

 v_MinPrice := v_BuyPrice * 1.05;
 v_discountedPrice := v_actualPrice * 0.95;

 IF (v_discountedPrice >= v_MinPrice) THEN
	UPDATE orderDetails
	SET priceEach = ROUND(v_discountedPrice, 2)
	WHERE orderNumber = v_OrderNo AND productCode = v_ProductCode;

	DBMS_OUTPUT.PUT_LINE('Paid ' || v_actualPrice);
	DBMS_OUTPUT.PUT_LINE('New Discounted Price ' || v_discountedPrice);
 ELSE
	DBMS_OUTPUT.PUT_LINE('Paid ' || v_actualPrice || ' for ' || v_ProductCode);
	DBMS_OUTPUT.PUT_LINE('Already lowest price. ' || 'No further discounts.');
 END IF;
  
EXCEPTION
  WHEN NO_DATA_FOUND THEN
    DBMS_OUTPUT.PUT_LINE('Unable to process discount request.');
    DBMS_OUTPUT.PUT_LINE('Order not found or already past 30 days.');
END;
/

EXEC prc_discount_claim(10425, 'S32_2509');

/*
Procedure created.

Customer last made an order on 28/02/2023 .
That was 0 days ago.
Paid 50.32
New Discounted Price 47.8

PL/SQL procedure successfully completed.
*/