SET SERVEROUTPUT ON

BEGIN
 NULL;
END;
/

/*
PL/SQL procedure successfully completed.
*/

-----

CREATE OR REPLACE PROCEDURE prc_hello1 IS 
BEGIN
 DBMS_OUTPUT.PUT_LINE('Good day !');
END;
/

/*
Procedure created.
*/

-----

EXEC prc_hello1

/*
Good day !

PL/SQL procedure successfully completed.
*/

-----

CREATE OR REPLACE PROCEDURE prc_hello2 (v_salutation IN VARCHAR, v_name IN VARCHAR) IS 
BEGIN
 DBMS_OUTPUT.PUT_LINE('Good day, ' || v_salutation || ' ' || v_name || ' !');
END;
/

EXEC prc_hello2 ('Mr.', 'Heng')

/*
Procedure created.

Good day, Mr. Heng !

PL/SQL procedure successfully completed.
*/

-----

COLUMN text FORMAT A50
SELECT line, text FROM user_source WHERE name = 'PRC_HELLO1';

/*
      LINE TEXT
---------- --------------------------------------------------
         1 PROCEDURE prc_hello1 IS
         2 BEGIN
         3  DBMS_OUTPUT.PUT_LINE('Good day !');
         4 END;
*/

-----

SELECT line, text FROM user_source WHERE name = 'PRC_HELLO2';

/*
      LINE TEXT
---------- --------------------------------------------------
         1 PROCEDURE prc_hello2 (v_salutation IN VARCHAR, v_n
           ame IN VARCHAR) IS

         2 BEGIN
         3  DBMS_OUTPUT.PUT_LINE('Good day, ' || v_salutation
            || ' ' || v_name || ' !');

         4 END;
*/

-----
SELECT DISTINCT name FROM user_source;

/*
NAME
------------------------------
PRC_HELLO2
SAMPLE1
PRC_HELLO1
*/

-----
CREATE OR REPLACE PROCEDURE prc_hello3 (v_salutation IN VARCHAR, v_name IN VARCHAR) IS 
v_day VARCHAR(10);
v_today DATE;

BEGIN
 v_day := TO_CHAR(SYSDATE, 'DAY');
 v_today := SYSDATE;
 DBMS_OUTPUT.PUT_LINE('Today is ' || v_today || ' (' || v_day || ' ).');
 DBMS_OUTPUT.PUT_LINE('Good day, ' || v_salutation || ' ' || v_name || ' !');
END;
/

EXEC prc_hello3 ('Mr.', 'Heng')

/*
Procedure created.

Today is 27-FEB-23 (MONDAY    ).
Good day, Mr. Heng !

PL/SQL procedure successfully completed.
*/

-----
CREATE OR REPLACE FUNCTION fun_getAge(v_dob IN DATE) RETURN NUMBER IS
BEGIN
 RETURN EXTRACT(YEAR FROM (Sysdate - v_dob) YEAR TO MONTH);
END;
/

EXEC

/*
Function created.
*/

-----
CREATE OR REPLACE PROCEDURE prc_hello4 (v_salutation IN VARCHAR, v_name IN VARCHAR, v_dob IN DATE) IS 
v_day VARCHAR(10);
v_today DATE;

BEGIN
 v_day := TO_CHAR(SYSDATE, 'DAY');
 v_today := SYSDATE;
 DBMS_OUTPUT.PUT_LINE('Today is ' || v_today || ' (' || v_day || ' ).');
 DBMS_OUTPUT.PUT_LINE('Good day, ' || v_salutation || ' ' || v_name || ' !');
 DBMS_OUTPUT.PUT_LINE('Today, you are ' || fun_getAge(v_dob) || ' years old.');
END;
/

EXEC prc_hello4 ('Mr.', 'Heng', '23-DEC-79')

/*
Procedure created.

Today is 27-FEB-23 (MONDAY    ).
Good day, Mr. Heng !
Today, you are 43 years old.

PL/SQL procedure successfully completed.
*/

-----
SET LINESIZE 120
SET PAGESIZE 100

SELECT *
FROM (SELECT O.CustomerNumber, MAX(OrderDate) as LastDay
	FROM Orders O, Customers C
	WHERE O.customerNumber = C.CustomerNumber
	GROUP BY O.CustomerNumber
	ORDER BY LastDay DESC )
WHERE ROWNUM <= 20;

/*
CUSTOMERNUMBER LASTDAY
-------------- ---------
           119 31-MAY-05
           141 31-MAY-05
           157 30-MAY-05
           314 30-MAY-05
           124 29-MAY-05
           282 29-MAY-05
           382 17-MAY-05
           412 16-MAY-05
           386 10-MAY-05
           471 09-MAY-05
           362 06-MAY-05
           175 05-MAY-05
           233 01-MAY-05
           357 29-APR-05
           166 23-APR-05
           398 22-APR-05
           450 22-APR-05
           145 15-APR-05
           209 14-APR-05
           201 08-APR-05

20 rows selected.
*/

-----
UPDATE Orders
SET OrderDate = ADD_MONTHS(OrderDate, 213),
	RequiredDate = ADD_MONTHS(RequiredDate, 213),
	ShippedDate = ADD_MONTHS(ShippedDate, 213);

SET LINESIZE 120
SET PAGESIZE 100

SELECT *
FROM (SELECT O.CustomerNumber, MAX(OrderDate) as LastDay
	FROM Orders O, Customers C
	WHERE O.customerNumber = C.CustomerNumber
	GROUP BY O.CustomerNumber
	ORDER BY LastDay DESC )
WHERE ROWNUM <= 20;

/*
326 rows updated.

CUSTOMERNUMBER LASTDAY
-------------- ---------
           119 28-FEB-23
           314 28-FEB-23
           282 28-FEB-23
           157 28-FEB-23
           141 28-FEB-23
           124 28-FEB-23
           382 17-FEB-23
           412 16-FEB-23
           386 10-FEB-23
           471 09-FEB-23
           362 06-FEB-23
           175 05-FEB-23
           233 01-FEB-23
           357 29-JAN-23
           166 23-JAN-23
           398 22-JAN-23
           450 22-JAN-23
           145 15-JAN-23
           209 14-JAN-23
           201 08-JAN-23

20 rows selected.
*/