/*
CREATE TABLE Customers1(
CustID	  CHAR(6) NOT NULL,
CustName  VARCHAR(30) NOT NULL,
CustCreditLimit NUMBER(6,2) default 1000.00,
CustCreditAmount NUMBER(6,2) default 0.00,
PRIMARY KEY (CustID)
);
*/

DROP TABLE InsertCustomers1;
DROP TABLE UpdateCustomers1;
DROP TABLE DeleteCustomers1;

CREATE TABLE InsertCustomers1(
CustID	  CHAR(6) NOT NULL,
CustName  VARCHAR(30) NOT NULL,
CustCreditLimit NUMBER(6,2) default 1000.00,
CustCreditAmount NUMBER(6,2) default 0.00,
UserID         VARCHAR(30),   
TransDate  DATE,
TransTime CHAR(8)
);

CREATE TABLE UpdateCustomers1(
CustID	  CHAR(6) NOT NULL,
CustName  VARCHAR(30) NOT NULL,
NewCustName  VARCHAR(30) NOT NULL,
CustCreditLimit NUMBER(6,2) default 1000.00,
NewCustCreditLimit NUMBER(6,2) default 1000.00,
CustCreditAmount NUMBER(6,2) default 0.00,
NewCustCreditAmount NUMBER(6,2) default 0.00,
UserID         VARCHAR(30),   
TransDate  DATE,
TransTime CHAR(8)
);

CREATE TABLE DeleteCustomers1(
CustID	  CHAR(6) NOT NULL,
CustName  VARCHAR(30) NOT NULL,
CustCreditLimit NUMBER(6,2) default 1000.00,
CustCreditAmount NUMBER(6,2) default 0.00,
UserID         VARCHAR(30),   
TransDate  DATE,
TransTime CHAR(8)
);