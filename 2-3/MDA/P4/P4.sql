/*Procedure: Single row results
Cursor: Multiple row results

In assignment: (part of extra efforts)
- if want to get more marks, tch expect to use nested cursor
- display custom exceptions (declare it at the top of procedure [empty_data EXCEPTION]) - do 1 then 2 marks
- display default exceptions (e.g.: NO_DATA_FOUND) - must have at least 2 then 2 marks

Cursor criteria:
- must set serveroutput on
*/

SET SERVEROUTPUT ON
SET linesize 120
SET pagesize 100
ALTER SESSION SET NLS_DATE_FORMAT = 'DD-MM-YYYY';

BEGIN
DBMS_OUTPUT.PUT_LINE(LPAD('Hello', 100, '*'));
DBMS_OUTPUT.PUT_LINE(CHR(10));
DBMS_OUTPUT.PUT_LINE(LPAD('Hello', 100, '='));
END;
/

/*
LPAD: left pad
CHR(10): empty row

Output:
***********************************************************************************************Hello


===============================================================================================Hello

PL/SQL procedure successfully completed.
*/

desc products

/*
 Name                                                              Null?    Type
 ----------------------------------------------------------------- -------- --------------------------------------------
 PRODUCTCODE                                                       NOT NULL VARCHAR2(15)
 PRODUCTNAME                                                       NOT NULL VARCHAR2(70)
 PRODUCTLINE                                                       NOT NULL VARCHAR2(50)
 PRODUCTSCALE                                                      NOT NULL VARCHAR2(10)
 PRODUCTVENDOR                                                     NOT NULL VARCHAR2(50)
 PRODUCTDESCRIPTION                                                NOT NULL VARCHAR2(4000)
 QUANTITYINSTOCK                                                   NOT NULL NUMBER(4)
 BUYPRICE                                                          NOT NULL NUMBER(7,2)
 MSRP                                                              NOT NULL NUMBER(7,2)
*/
CREATE OR REPLACE PROCEDURE prc_show_product_by_id (v_code IN VARCHAR) IS
v_prodCode products.productCode%TYPE;
v_prodName products.productName%TYPE;
v_prodVendor products.productVendor%TYPE;
v_qty products.quantityInStock%TYPE;
v_buyPrice products.buyPrice%TYPE;

BEGIN
 SELECT productCode, productName, productVendor, quantityInStock, buyPrice INTO
        v_prodCode, v_prodName, v_prodVendor, v_qty, v_buyPrice
 FROM Products
 WHERE productCode = v_code;

 DBMS_OUTPUT.PUT_LINE(LPAD('=', 100, '='));
 DBMS_OUTPUT.PUT_LINE(CHR(10));
 DBMS_OUTPUT.PUT_LINE('Product supply by vendor: ' || v_prodVendor);
 DBMS_OUTPUT.PUT_LINE(CHR(10));
 DBMS_OUTPUT.PUT_LINE(LPAD('=', 100, '='));
 DBMS_OUTPUT.PUT_LINE(RPAD('Code', 10, ' ') || ' '||
                      RPAD('Product Name', 40, ' ') || ' '||
                      RPAD('Quantity', 10, ' ') || ' '||
                      RPAD('     Buy Price', 14, ' '));
 DBMS_OUTPUT.PUT_LINE(LPAD('=', 100, '='));

 DBMS_OUTPUT.PUT_LINE(RPAD(v_prodCode, 10, ' ') || ' '||
                      RPAD(v_prodName, 40, ' ') || ' '||
                      RPAD(v_qty, 10, ' ') || ' '||
                      RPAD(TO_CHAR(v_buyPrice, '$99,999.99'), 14, ' '));

 DBMS_OUTPUT.PUT_LINE(LPAD('=', 44, '=') || RPAD('End of File', 56, '='));

EXCEPTION
 WHEN NO_DATA_FOUND THEN
   DBMS_OUTPUT.PUT_LINE('No such product code: ' || v_code);
END;
/

EXEC prc_show_product_by_id ('S10_1678')
/*
%TYPE - no need to remember what data type / use previous defined data type

Output:
====================================================================================================


Product supply by vendor: Min Lin Diecast


====================================================================================================
Code       Product Name                             Quantity   Buy Price
====================================================================================================
S10_1678   1969 Harley Davidson Ultimate Chopper    7933            $48.81
============================================End of File=============================================

PL/SQL procedure successfully completed.
*/
CREATE OR REPLACE PROCEDURE prc_show_product_by_vendorSC1 (v_vendor IN VARCHAR) IS
v_prodCode products.productCode%TYPE;
v_prodName products.productName%TYPE;
v_prodVendor products.productVendor%TYPE;
v_qty products.quantityInStock%TYPE;
v_buyPrice products.buyPrice%TYPE;

CURSOR prodCursor IS
 SELECT productCode, productName, quantityInStock, buyPrice
 FROM Products
 WHERE productVendor = v_vendor;

BEGIN

 DBMS_OUTPUT.PUT_LINE(LPAD('=', 100, '='));
 DBMS_OUTPUT.PUT_LINE(CHR(10));
 DBMS_OUTPUT.PUT_LINE('Product supply by vendor: ' || v_vendor);
 DBMS_OUTPUT.PUT_LINE(CHR(10));
 DBMS_OUTPUT.PUT_LINE(LPAD('=', 100, '='));
 DBMS_OUTPUT.PUT_LINE(RPAD('Code', 10, ' ') || ' '||
                      RPAD('Product Name', 40, ' ') || ' '||
                      RPAD('Quantity', 10, ' ') || ' '||
                      RPAD('     Buy Price', 14, ' '));
 DBMS_OUTPUT.PUT_LINE(LPAD('=', 100, '='));

OPEN prodCursor;
LOOP
FETCH prodCursor INTO v_prodCode, v_prodName, v_qty, v_buyPrice;
IF(prodCursor%ROWCOUNT = 0) THEN
 DBMS_OUTPUT.PUT_LINE('No such product: ' || v_vendor);
END IF;
EXIT WHEN prodCursor%NOTFOUND;

 DBMS_OUTPUT.PUT_LINE(RPAD(v_prodCode, 10, ' ') || ' '||
                      RPAD(v_prodName, 40, ' ') || ' '||
                      RPAD(v_qty, 10, ' ') || ' '||
                      RPAD(TO_CHAR(v_buyPrice, '$99,999.99'), 14, ' '));
END LOOP;
 DBMS_OUTPUT.PUT_LINE(LPAD('=', 100, '='));
 DBMS_OUTPUT.PUT_LINE('Total number of product: ' || prodCursor%ROWCOUNT);
 DBMS_OUTPUT.PUT_LINE(LPAD('=', 44, '=') || RPAD('End of File', 56, '='));

CLOSE prodCursor;
END;
/
EXEC prc_show_product_by_vendorSC1 ('Min Lin Diecast')

/*
prodCursor%ROWCOUNT = 0 - means if no products found

Output:
====================================================================================================


Product supply by vendor: Min Lin Diecast


====================================================================================================
Code       Product Name                             Quantity        Buy Price
====================================================================================================
S18_1984   1995 Honda Civic                         9772            $93.89
S18_2957   1934 Ford V8 Coupe                       5649            $34.35
S18_3029   1999 Yamaha Speed Boat                   4259            $51.61
S18_3278   1969 Dodge Super Bee                     1917            $49.05
S18_4027   1970 Triumph Spitfire                    5545            $91.92
S700_1691  American Airlines: B767-300              5841            $51.15
S24_3151   1912 Ford Model T Delivery Wagon         9173            $46.91
S10_1678   1969 Harley Davidson Ultimate Chopper    7933            $48.81
====================================================================================================
Total number of product: 8
============================================End of File=============================================

PL/SQL procedure successfully completed.
*/
CREATE OR REPLACE PROCEDURE prc_show_product_by_vendorSC2 (v_vendor IN VARCHAR) IS

CURSOR prodCursor IS
 SELECT productCode, productName, quantityInStock, buyPrice
 FROM Products
 WHERE productVendor = v_vendor;

prodRec prodCursor%ROWTYPE;

BEGIN

 DBMS_OUTPUT.PUT_LINE(LPAD('=', 100, '='));
 DBMS_OUTPUT.PUT_LINE(CHR(10));
 DBMS_OUTPUT.PUT_LINE('Product supply by vendor: ' || v_vendor);
 DBMS_OUTPUT.PUT_LINE(CHR(10));
 DBMS_OUTPUT.PUT_LINE(LPAD('=', 100, '='));
 DBMS_OUTPUT.PUT_LINE(RPAD('Code', 10, ' ') || ' '||
                      RPAD('Product Name', 40, ' ') || ' '||
                      RPAD('Quantity', 10, ' ') || ' '||
                      RPAD('     Buy Price', 14, ' '));
 DBMS_OUTPUT.PUT_LINE(LPAD('=', 100, '='));

OPEN prodCursor;
LOOP
FETCH prodCursor INTO prodRec;
IF(prodCursor%ROWCOUNT = 0) THEN
 DBMS_OUTPUT.PUT_LINE('No such product: ' || v_vendor);
END IF;
EXIT WHEN prodCursor%NOTFOUND;

 DBMS_OUTPUT.PUT_LINE(RPAD(prodRec.productCode, 10, ' ') || ' '||
                      RPAD(prodRec.productName, 40, ' ') || ' '||
                      RPAD(prodRec.quantityInStock, 10, ' ') || ' '||
                      RPAD(TO_CHAR(prodRec.buyPrice, '$99,999.99'), 14, ' '));
END LOOP;
 DBMS_OUTPUT.PUT_LINE(LPAD('=', 100, '='));
 DBMS_OUTPUT.PUT_LINE('Total number of product: ' || prodCursor%ROWCOUNT);
 DBMS_OUTPUT.PUT_LINE(LPAD('=', 44, '=') || RPAD('End of File', 56, '='));

CLOSE prodCursor;
END;
/
EXEC prc_show_product_by_vendorSC1 ('Min Lin Diecast')
/*
%TYPE - display one column in one type
%ROWTYPE - display a lot of columns with different types

Output:
====================================================================================================


Product supply by vendor: Min Lin Diecast


====================================================================================================
Code       Product Name                             Quantity        Buy Price
====================================================================================================
S18_1984   1995 Honda Civic                         9772            $93.89
S18_2957   1934 Ford V8 Coupe                       5649            $34.35
S18_3029   1999 Yamaha Speed Boat                   4259            $51.61
S18_3278   1969 Dodge Super Bee                     1917            $49.05
S18_4027   1970 Triumph Spitfire                    5545            $91.92
S700_1691  American Airlines: B767-300              5841            $51.15
S24_3151   1912 Ford Model T Delivery Wagon         9173            $46.91
S10_1678   1969 Harley Davidson Ultimate Chopper    7933            $48.81
====================================================================================================
Total number of product: 8
============================================End of File=============================================

PL/SQL procedure successfully completed.
*/
CREATE OR REPLACE PROCEDURE prc_show_product_by_vendorNC1 IS
v_prodCode products.productCode%Type;
v_prodName products.productName%Type;
v_prodVendor products.productVendor%Type;
v_qty products.quantityInStock%Type;
v_buyPrice products.buyPrice%Type;
v_subtotal NUMBER(11,2);
v_grandTotal NUMBER(11,2);
v_totalValue NUMBER(15,2);

CURSOR vendorCursor IS
 SELECT DISTINCT productVendor
 FROM products
 ORDER BY productVendor;

CURSOR prodCursor IS
 SELECT productCode, productName, quantityInStock, buyPrice, quantityInStock * buyPrice AS subtotal
 FROM Products
 WHERE productVendor = v_prodVendor;

BEGIN
v_totalValue := 0;
OPEN vendorCursor;

LOOP
 FETCH vendorCursor INTO v_prodVendor;
 EXIT WHEN vendorCursor%NOTFOUND;

  DBMS_OUTPUT.PUT_LINE(LPAD('=', 100, '='));
  DBMS_OUTPUT.PUT_LINE(CHR(10));
  DBMS_OUTPUT.PUT_LINE('Product supply by vendor: ' || v_prodVendor);
  DBMS_OUTPUT.PUT_LINE(CHR(10));
  DBMS_OUTPUT.PUT_LINE(LPAD('=', 100, '='));
  DBMS_OUTPUT.PUT_LINE(RPAD('Code', 10, ' ') || ' '||
               RPAD('Product Name', 40, ' ') || ' '||
               RPAD('Quantity', 10, ' ') || ' '||
               RPAD('     Buy Price', 14, ' ') || ' '||
               RPAD('     Subtotal', 17, ' '));
  DBMS_OUTPUT.PUT_LINE(LPAD('=', 100, '='));

 v_grandTotal := 0;
 OPEN prodCursor;
 LOOP 
  FETCH prodCursor INTO v_prodCode, v_prodName, v_qty, v_buyPrice, v_subtotal; 
  IF(prodCursor%ROWCOUNT = 0) THEN
     DBMS_OUTPUT.PUT_LINE('No such product vendor: ' || v_prodVendor);
  END IF;
  EXIT WHEN prodCursor%NOTFOUND;

  v_grandTotal := v_grandTotal + v_subtotal;

  DBMS_OUTPUT.PUT_LINE(RPAD(v_prodCode, 10, ' ') || ' '||
                RPAD(v_prodName, 40, ' ') || ' '||
                RPAD(v_qty, 10, ' ') || ' '||
                RPAD(TO_CHAR(v_buyPrice, '$99,999.99'), 14, ' ')|| ' '||
                RPAD(TO_CHAR(v_subtotal, '$9,999,999.99'), 17, ' '));
  END LOOP;
  DBMS_OUTPUT.PUT_LINE(LPAD('=', 100, '='));
  DBMS_OUTPUT.PUT_LINE('Total number of product: ' || prodCursor%ROWCOUNT);
  DBMS_OUTPUT.PUT_LINE('Grand Total: ' || TO_CHAR(v_grandTotal, '$999,999,999.99'));
  DBMS_OUTPUT.PUT_LINE(LPAD('=', 100, '='));
 
v_totalValue := v_totalValue + v_grandTotal;
 CLOSE prodCursor;
END LOOP;
DBMS_OUTPUT.PUT_LINE('Total number of vendor: ' || vendorCursor%ROWCOUNT);
DBMS_OUTPUT.PUT_LINE('Total Value: ' || TO_CHAR(v_totalValue, '$999,999,999.99'));
CLOSE vendorCursor;
DBMS_OUTPUT.PUT_LINE(LPAD('=', 44, '=') || RPAD('End Of File', 56, '='));
END;
/

EXEC prc_show_product_by_vendorNC1
/*
Output:
====================================================================================================


Product supply by vendor: Autoart Studio Design


====================================================================================================
Code       Product Name                             Quantity        Buy Price      Subtotal
====================================================================================================
S18_2325   1932 Model A Ford J-Coupe                9354            $58.48       $547,021.92
S50_4713   2002 Yamaha YZR M1                       600             $34.17        $20,502.00
S700_1138  The Schooner Bluenose                    1897            $34.00        $64,498.00
S24_1578   1997 BMW R 1100 S                        7003            $60.86       $426,202.58
S24_2300   1962 Volkswagen Microbus                 2327            $61.34       $142,738.18
S24_2841   1900s Vintage Bi-Plane                   5942            $34.25       $203,513.50
S24_3420   1937 Horch 930V Limousine                2902            $26.30        $76,322.60
S12_1099   1968 Ford Mustang                        68              $95.34         $6,483.12
====================================================================================================
Total number of product: 8
Grand Total:    $1,487,281.90
====================================================================================================
====================================================================================================


Product supply by vendor: Carousel DieCast Legends


====================================================================================================
Code       Product Name                             Quantity        Buy Price      Subtotal
====================================================================================================
S18_2432   1926 Ford Fire Engine                    2018            $24.92        $50,288.56
S18_2949   1913 Ford Model T Speedster              4189            $60.78       $254,607.42
S18_3259   Collectable Wooden Train                 6450            $67.56       $435,762.00
S700_2824  1982 Camaro Z28                          6934            $46.53       $322,639.02
S700_3505  The Titanic                              1956            $51.09        $99,932.04
S24_1628   1966 Shelby Cobra 427 S/C                8197            $29.18       $239,188.46
S24_2011   18th century schooner                    1898            $82.34       $156,281.32
S24_2840   1958 Chevy Corvette Limited Edition      2542            $15.91        $40,443.22
S24_3816   1940 Ford Delivery Sedan                 6621            $48.64       $322,045.44
====================================================================================================
Total number of product: 9
Grand Total:    $1,921,187.48
====================================================================================================
====================================================================================================


Product supply by vendor: Classic Metal Creations


====================================================================================================
Code       Product Name                             Quantity        Buy Price      Subtotal
====================================================================================================
S18_4721   1957 Corvette Convertible                1249            $69.93        $87,342.57
S24_4620   1961 Chevrolet Impala                    7869            $32.33       $254,404.77
S32_2509   1954 Greyhound Scenicruiser              2874            $25.98        $74,666.52
S50_1514   1962 City of Detroit Streetcar           1645            $37.49        $61,671.05
S24_1785   1928 British Royal Navy Airplane         3627            $66.74       $242,065.98
S24_2022   1938 Cadillac V-16 Presidential Limousin 2847            $20.61        $58,676.67
S24_2766   1949 Jaguar XK 120                       2350            $47.25       $111,037.50
S24_3856   1956 Porsche 356A Coupe                  6600            $98.30       $648,780.00
S10_1949   1952 Alpine Renault 1300                 7305            $98.58       $720,126.90
S18_1589   1965 Aston Martin DB5                    9042            $65.96       $596,410.32
====================================================================================================
Total number of product: 10
Grand Total:    $2,855,182.28
====================================================================================================
====================================================================================================


Product supply by vendor: Exoto Designs


====================================================================================================
Code       Product Name                             Quantity        Buy Price      Subtotal
====================================================================================================
S18_3320   1917 Maxwell Touring Car                 7913            $57.54       $455,314.02
S18_3856   1941 Chevrolet Special Deluxe Cabriolet  2378            $64.58       $153,571.24
S18_4409   1932 Alfa Romeo 8C2300 Spider Sport      6553            $43.26       $283,482.78
S18_4522   1904 Buick Runabout                      8290            $52.66       $436,551.40
S24_4048   1992 Porsche Cayenne Turbo Silver        6582            $69.78       $459,291.96
S32_1374   1997 BMW F650 ST                         178             $66.92        $11,911.76
S24_2887   1952 Citroen-15CV                        1452            $72.82       $105,734.64
S24_3191   1969 Chevrolet Camaro Z28                4695            $50.51       $237,144.45
S12_4473   1957 Chevy Pickup                        6125            $55.70       $341,162.50
====================================================================================================
Total number of product: 9
Grand Total:    $2,484,164.75
====================================================================================================
====================================================================================================


Product supply by vendor: Gearbox Collectibles


====================================================================================================
Code       Product Name                             Quantity        Buy Price      Subtotal
====================================================================================================
S18_2238   1998 Chrysler Plymouth Prowler           4724           $101.51       $479,533.24
S18_2581   P-51-D Mustang                           992             $49.00        $48,608.00
S18_2795   1928 Mercedes-Benz SSK                   548             $72.56        $39,762.88
S18_3482   1976 Ford Gran Torino                    9127            $73.49       $670,743.23
S18_3685   1948 Porsche Type 356 Roadster           8990            $62.16       $558,818.40
S32_2206   1982 Ducati 996 R                        9241            $24.14       $223,077.74
S32_3207   1950's Chicago Surface Lines Streetcar   8601            $26.72       $229,818.72
S24_3432   2002 Chevy Corvette                      9446            $62.11       $586,691.06
S18_1889   1948 Porsche 356-A Roadster              8826            $53.90       $475,721.40
====================================================================================================
Total number of product: 9
Grand Total:    $3,312,774.67
====================================================================================================
====================================================================================================


Product supply by vendor: Highway 66 Mini Classics


====================================================================================================
Code       Product Name                             Quantity        Buy Price      Subtotal
====================================================================================================
S18_3233   1985 Toyota Supra                        7733            $57.01       $440,858.33
S32_4289   1928 Ford Phaeton Deluxe                 136             $33.02         $4,490.72
S50_1392   Diamond T620 Semi-Skirted Tanker         1016            $68.29        $69,382.64
S700_2834  ATA: B757-300                            7106            $59.33       $421,598.98
S24_1444   1970 Dodge Coronet                       4074            $32.37       $131,875.38
S24_2000   1960 BSA Gold Star DBD34                 15              $37.32           $559.80
S24_2360   1982 Ducati 900 Monster                  6840            $47.10       $322,164.00
S10_2016   1996 Moto Guzzi 1100i                    6625            $68.99       $457,058.75
S18_1129   1993 Mazda RX-7                          3975            $83.51       $331,952.25
====================================================================================================
Total number of product: 9
Grand Total:    $2,179,940.85
====================================================================================================
====================================================================================================


Product supply by vendor: Min Lin Diecast


====================================================================================================
Code       Product Name                             Quantity        Buy Price      Subtotal
====================================================================================================
S18_1984   1995 Honda Civic                         9772            $93.89       $917,493.08
S18_2957   1934 Ford V8 Coupe                       5649            $34.35       $194,043.15
S18_3029   1999 Yamaha Speed Boat                   4259            $51.61       $219,806.99
S18_3278   1969 Dodge Super Bee                     1917            $49.05        $94,028.85
S18_4027   1970 Triumph Spitfire                    5545            $91.92       $509,696.40
S700_1691  American Airlines: B767-300              5841            $51.15       $298,767.15
S24_3151   1912 Ford Model T Delivery Wagon         9173            $46.91       $430,305.43
S10_1678   1969 Harley Davidson Ultimate Chopper    7933            $48.81       $387,209.73
====================================================================================================
Total number of product: 8
Grand Total:    $3,051,350.78
====================================================================================================
====================================================================================================


Product supply by vendor: Motor City Art Classics


====================================================================================================
Code       Product Name                             Quantity        Buy Price      Subtotal
====================================================================================================
S18_2248   1911 Ford Town Car                       540             $33.30        $17,982.00
S18_4600   1940s Ford truck                         3128            $84.76       $265,129.28
S32_1268   1980Æs GM Manhattan Express              5099            $53.93       $274,989.07
S700_2466  America West Airlines B757-200           9653            $68.80       $664,126.40
S700_3167  F/A 18 Hornet 1/72                       551             $54.40        $29,974.40
S72_1253   Boeing X-32A JSF                         4857            $32.77       $159,163.89
S24_1937   1939 Chevrolet Deluxe Coupe              7332            $22.57       $165,483.24
S10_4757   1972 Alfa Romeo GTA                      3252            $85.68       $278,631.36
S18_1342   1937 Lincoln Berline                     8693            $60.62       $526,969.66
====================================================================================================
Total number of product: 9
Grand Total:    $2,382,449.30
====================================================================================================
====================================================================================================


Product supply by vendor: Red Start Diecast


====================================================================================================
Code       Product Name                             Quantity        Buy Price      Subtotal
====================================================================================================
S18_2870   1999 Indy 500 Monte Carlo SS             8164            $56.76       $463,388.64
S18_3136   18th Century Vintage Horse Carriage      5992            $60.74       $363,954.08
S32_3522   1996 Peterbilt 379 Stake Bed with Outrig 814             $33.61        $27,358.54
S700_2610  The USS Constitution Ship                7083            $33.97       $240,609.51
S24_3969   1936 Mercedes Benz 500k Roadster         2081            $21.75        $45,261.75
S10_4698   2003 Harley-Davidson Eagle Drag Bike     5582            $91.02       $508,073.64
S18_1662   1980s Black Hawk Helicopter              5330            $77.27       $411,849.10
====================================================================================================
Total number of product: 7
Grand Total:    $2,060,495.26
====================================================================================================
====================================================================================================


Product supply by vendor: Second Gear Diecast


====================================================================================================
Code       Product Name                             Quantity        Buy Price      Subtotal
====================================================================================================
S24_4258   1936 Chrysler Airflow                    4710            $57.46       $270,636.60
S32_4485   1974 Ducati 350 Mk3 Desmo                3341            $56.13       $187,530.33
S700_4002  American Airlines: MD-11S                8820            $36.27       $319,901.40
S24_2972   1982 Lamborghini Diablo                  7723            $16.24       $125,421.52
S24_3949   Corsair F4U ( Bird Cage)                 6812            $29.34       $199,864.08
S10_4962   1962 LanciaA Delta 16V                   6791           $103.42       $702,325.22
S12_1108   2001 Ferrari Enzo                        3619            $95.59       $345,940.21
S12_3891   1969 Ford Falcon                         1049            $83.05        $87,119.45
====================================================================================================
Total number of product: 8
Grand Total:    $2,238,738.81
====================================================================================================
====================================================================================================


Product supply by vendor: Studio M Art Models


====================================================================================================
Code       Product Name                             Quantity        Buy Price      Subtotal
====================================================================================================
S18_3782   1957 Vespa GS150                         7689            $32.95       $253,352.55
S18_4668   1939 Cadillac Limousine                  6645            $23.14       $153,765.30
S50_1341   1930 Buick Marquette Phaeton             7062            $27.06       $191,097.72
S700_1938  The Mayflower                            737             $43.30        $31,912.10
S18_4933   1957 Ford Thunderbird                    3209            $34.21       $109,779.89
S12_3990   1970 Plymouth Hemi Cuda                  5663            $31.92       $180,762.96
S18_1097   1940 Ford Pickup Truck                   2613            $58.33       $152,416.29
S18_1367   1936 Mercedes-Benz 500K Special Roadster 8635            $24.26       $209,485.10
====================================================================================================
Total number of product: 8
Grand Total:    $1,282,571.91
====================================================================================================
====================================================================================================


Product supply by vendor: Unimax Art Galleries


====================================================================================================
Code       Product Name                             Quantity        Buy Price      Subtotal
====================================================================================================
S18_2319   1964 Mercedes Tour Bus                   8258            $74.86       $618,193.88
S18_3140   1903 Ford Model A                        3913            $68.30       $267,257.90
S18_3232   1992 Ferrari 360 Spider red              8347            $77.90       $650,231.30
S24_4278   1900s Vintage Tri-Plane                  2756            $36.23        $99,849.88
S700_2047  HMS Bounty                               3501            $39.83       $139,444.83
S72_3212   Pont Yacht                               414             $33.30        $13,786.20
S24_1046   1970 Chevy Chevelle SS 454               1005            $49.24        $49,486.20
S12_2823   2002 Suzuki XREO                         9997            $66.27       $662,501.19
====================================================================================================
Total number of product: 8
Grand Total:    $2,500,751.38
====================================================================================================
====================================================================================================


Product supply by vendor: Welly Diecast Productions


====================================================================================================
Code       Product Name                             Quantity        Buy Price      Subtotal
====================================================================================================
S18_2625   1936 Harley Davidson El Knucklehead      4357            $24.23       $105,570.11
S700_3962  The Queen Mary                           5088            $53.63       $272,869.44
S24_3371   1971 Alpine Renault 1600s                7995            $38.58       $308,447.10
S12_1666   1958 Setra Bus                           1579            $77.90       $123,004.10
S12_3148   1969 Corvair Monza                       6906            $89.14       $615,600.84
S12_3380   1968 Dodge Charger                       9123            $75.16       $685,684.68
S12_4675   1969 Dodge Charger                       7323            $58.73       $430,079.79
S18_1749   1917 Grand Touring Sedan                 2724            $86.70       $236,170.80
====================================================================================================
Total number of product: 8
Grand Total:    $2,777,426.86
====================================================================================================
Total number of vendor: 13
Total Value:   $30,534,316.23
============================================End Of File=============================================

PL/SQL procedure successfully completed.
*/