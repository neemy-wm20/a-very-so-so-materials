/*
testing (multiple line command)
*/

--testing (single line command)

SET linesize 120;
SET pagesize 100;

ALTER SESSION SET NLS_DATE_FORMAT = 'DD-MON-YYYY';

COLUMN OrderNumber     FORMAT 99999999      HEADING 'Order No'
COLUMN ProductCode     FORMAT A20           HEADING 'Code'
COLUMN QuantityOrdered FORMAT 99999         HEADING 'Quantity'
COLUMN PriceEach       FORMAT $99,999.99    HEADING 'Price'
COLUMN Subtotal        FORMAT $9,999,999.99 HEADING 'Subtotal'

--A20: means 20 characters

TTITLE CENTER 'Order Details for ' _DATE -
LEFT 'Page: ' FORMAT 999 SQL.PNO SKIP 2

BREAK ON OrderNumber SKIP 2
--MERGE same order numbers, only first line is shown

SELECT OrderNumber, ProductCode, QuantityOrdered, PriceEach,
       QuantityOrdered * PriceEach AS Subtotal
FROM   OrderDetails;

TTITLE OFF
CLEAR COLUMNS
CLEAR BREAKS