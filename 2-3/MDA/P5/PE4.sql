select productLine from productlines;

SELECT DISTINCT productLine
FROM products;

/*
--Both commands above display this same result

PRODUCTLINE
--------------------------------------------------
Classic Cars
Motorcycles
Planes
Ships
Trains
Trucks and Buses
Vintage Cars

7 rows selected.
*/
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
/* (1) Write a procedure to print all products for a given product line. The procedure will receive
productLine as input. You should print useful relevant information. */
SET serveroutput ON
SET linesize 120
SET pagesize 100
ALTER SESSION SET NLS_DATE_FORMAT = 'DD-MM-YYYY';

CREATE OR REPLACE PROCEDURE prc_show_product_by_prodLine (v_prodLine IN VARCHAR) IS

CURSOR prodCursor IS
 SELECT productCode, productName, productVendor, quantityInStock, buyPrice
 FROM products
 WHERE productLine = v_prodLine;

prodRec prodCursor %ROWTYPE;

BEGIN
/* Title */
DBMS_OUTPUT.PUT_LINE(RPAD('=', 120, '='));
DBMS_OUTPUT.PUT_LINE(CHR(10));
DBMS_OUTPUT.PUT_LINE('Product on Product Line: ' || v_prodLine);
DBMS_OUTPUT.PUT_LINE(CHR(10));
DBMS_OUTPUT.PUT_LINE(RPAD('=', 120, '='));

/* Heading */
DBMS_OUTPUT.PUT_LINE(RPAD('Code', 10, ' ') || ' ' ||
                     RPAD('Product Name', 40, ' ') || ' ' ||
                     RPAD('Product Vendor', 30, ' ') || ' ' ||
                     RPAD('Quantity', 10, ' ') || ' ' ||
                     RPAD('     Buy Price', 20, ' '));
DBMS_OUTPUT.PUT_LINE(RPAD('=', 120, '='));

/* Data */
OPEN prodCursor;
LOOP
FETCH prodCursor INTO prodRec;
IF (prodCursor%ROWCOUNT = 0) THEN
 DBMS_OUTPUT.PUT_LINE('No such product line: ' || v_prodLine);
END IF;
EXIT WHEN prodCursor%NOTFOUND;

DBMS_OUTPUT.PUT_LINE(RPAD(prodRec.productCode, 10, ' ') || ' ' ||
                     RPAD(prodRec.productName, 40, ' ') || ' ' ||
                     RPAD(prodRec.productVendor, 30, ' ') || ' ' ||
                     RPAD(prodRec.quantityInStock, 10, ' ') || ' ' ||
                     RPAD(TO_CHAR(prodRec.buyPrice, '$99,999.99'), 20, ' '));
END LOOP;

DBMS_OUTPUT.PUT_LINE(RPAD('=', 120, '='));
DBMS_OUTPUT.PUT_LINE('Total number of products: ' || prodCursor%ROWCOUNT);
DBMS_OUTPUT.PUT_LINE(RPAD('=', 54, '=') || RPAD('End of File', 66, '='));

CLOSE prodCursor;
END;
/
EXEC prc_show_product_by_prodLine ('Planes')

/*
========================================================================================================================


Product on Product Line: Planes


========================================================================================================================
Code       Product Name                             Product Vendor                 Quantity        Buy Price
========================================================================================================================
S18_2581   P-51-D Mustang                           Gearbox Collectibles           992             $49.00
S24_4278   1900s Vintage Tri-Plane                  Unimax Art Galleries           2756            $36.23
S700_1691  American Airlines: B767-300              Min Lin Diecast                5841            $51.15
S700_2466  America West Airlines B757-200           Motor City Art Classics        9653            $68.80
S700_2834  ATA: B757-300                            Highway 66 Mini Classics       7106            $59.33
S700_3167  F/A 18 Hornet 1/72                       Motor City Art Classics        551             $54.40
S700_4002  American Airlines: MD-11S                Second Gear Diecast            8820            $36.27
S72_1253   Boeing X-32A JSF                         Motor City Art Classics        4857            $32.77
S24_1785   1928 British Royal Navy Airplane         Classic Metal Creations        3627            $66.74
S24_2841   1900s Vintage Bi-Plane                   Autoart Studio Design          5942            $34.25
S24_3949   Corsair F4U ( Bird Cage)                 Second Gear Diecast            6812            $29.34
S18_1662   1980s Black Hawk Helicopter              Red Start Diecast              5330            $77.27
========================================================================================================================
Total number of products: 12
======================================================End of File=======================================================

PL/SQL procedure successfully completed.
*/
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
/* (2) Write a procedure to list all order details for each order for a particular date range of OrderDate.
Calculate and print the total value of all the orders for that date range. */
CREATE OR REPLACE PROCEDURE prc_orderDetails_by_dateRange (v_startDate IN DATE, v_endDate IN DATE) IS
v_orderNumber Orders.orderNumber%TYPE;
v_grandTotal NUMBER(11, 2);
v_totalValue NUMBER(15, 2);

CURSOR orderCursor IS
 SELECT orderNumber
 FROM orders
 WHERE orderDate BETWEEN v_startDate AND v_endDate
 ORDER BY orderNumber;

CURSOR orderDetailsCursor IS
 SELECT OD.productCode, productName, quantityOrdered, priceEach, quantityOrdered * priceEach AS Subtotal
 FROM Products P, OrderDetails OD
 WHERE P.ProductCode = OD.ProductCode AND orderNumber = v_orderNumber;

orderDetailsRec orderDetailsCursor%ROWTYPE;

BEGIN
/* Outer loop */
OPEN orderCursor;
v_totalValue := 0;
LOOP
FETCH orderCursor INTO v_orderNumber;
EXIT WHEN orderCursor%NOTFOUND;

/* Title */
DBMS_OUTPUT.PUT_LINE(RPAD('=', 120, '='));
DBMS_OUTPUT.PUT_LINE(CHR(10));
DBMS_OUTPUT.PUT_LINE('Order Number: ' || v_orderNumber);
DBMS_OUTPUT.PUT_LINE(CHR(10));
DBMS_OUTPUT.PUT_LINE(RPAD('=', 120, '='));

/* Heading */
DBMS_OUTPUT.PUT_LINE(RPAD('Code', 10, ' ') || ' '||
                     RPAD('Product Name', 40, ' ') || ' '||
                     RPAD('Quantity', 10, ' ') || ' '||
                     RPAD('    Price', 20, ' ') || ' '||
                     RPAD('   Subtotal', 20, ' '));
DBMS_OUTPUT.PUT_LINE(RPAD('=', 120, '='));

/* Data */
  /* Inner loop */
  OPEN orderDetailsCursor;
  v_grandTotal := 0;
  LOOP
  FETCH orderDetailsCursor INTO orderDetailsRec;
  EXIT WHEN orderDetailsCursor%NOTFOUND;

  v_grandTotal := v_grandTotal + orderDetailsRec.Subtotal;

  DBMS_OUTPUT.PUT_LINE(RPAD(orderDetailsRec.productCode, 10, ' ') || ' '||
                       RPAD(orderDetailsRec.productName, 40, ' ') || ' '||
                       RPAD(orderDetailsRec.quantityOrdered, 10, ' ') || ' '||
                       RPAD(TO_CHAR(orderDetailsRec.priceEach, '$99,999.99'), 20, ' ') || ' '||
                       RPAD(TO_CHAR(orderDetailsRec.Subtotal, '$9,999,999.99'), 20, ' '));
  END LOOP;

  v_totalValue := v_totalValue + v_grandTotal;

/* Ending */
  DBMS_OUTPUT.PUT_LINE(RPAD('=', 120, '='));
  DBMS_OUTPUT.PUT_LINE('Total number of products per order: ' || orderDetailsCursor%ROWCOUNT);
  DBMS_OUTPUT.PUT_LINE('Grand Total: ' || TO_CHAR(v_grandTotal, '$9,999,999.99'));
  DBMS_OUTPUT.PUT_LINE(RPAD('=', 120, '='));
  CLOSE orderDetailsCursor; 

 END LOOP;
 DBMS_OUTPUT.PUT_LINE(CHR(10));
 DBMS_OUTPUT.PUT_LINE('Total number of orders: ' || orderCursor%ROWCOUNT);
 DBMS_OUTPUT.PUT_LINE('Total Value: ' || TO_CHAR(v_totalValue, '$9,999,999.99'));
 DBMS_OUTPUT.PUT_LINE(LPAD('=', 54, '=') || RPAD('End Of File', 66, '='));
 CLOSE orderCursor;
END;
/
EXEC prc_orderDetails_by_dateRange('01-02-2023', '28-02-2023')


/*
========================================================================================================================


Order Number: 10411


========================================================================================================================
Code       Product Name                             Quantity       Price               Subtotal
========================================================================================================================
S18_4600   1940s Ford truck                         46             $106.55               $4,901.30
S18_4668   1939 Cadillac Limousine                  35              $41.25               $1,443.75
S32_1268   1980Æs GM Manhattan Express              26              $78.01               $2,028.26
S32_3522   1996 Peterbilt 379 Stake Bed with Outrig 27              $60.76               $1,640.52
S700_2824  1982 Camaro Z28                          34              $89.01               $3,026.34
S10_1949   1952 Alpine Renault 1300                 23             $205.73               $4,731.79
S10_4962   1962 LanciaA Delta 16V                   27             $144.79               $3,909.33
S12_1666   1958 Setra Bus                           40             $110.70               $4,428.00
S18_1097   1940 Ford Pickup Truck                   27             $109.67               $2,961.09
========================================================================================================================
Total number of products per order: 9
Grand Total:     $29,070.38
========================================================================================================================
========================================================================================================================


Order Number: 10412


========================================================================================================================
Code       Product Name                             Quantity       Price               Subtotal
========================================================================================================================
S18_2238   1998 Chrysler Plymouth Prowler           41             $150.63               $6,175.83
S18_2319   1964 Mercedes Tour Bus                   56             $120.28               $6,735.68
S18_2432   1926 Ford Fire Engine                    47              $49.83               $2,342.01
S18_3232   1992 Ferrari 360 Spider red              60             $157.49               $9,449.40
S24_4048   1992 Porsche Cayenne Turbo Silver        31             $108.82               $3,373.42
S32_2509   1954 Greyhound Scenicruiser              19              $50.86                 $966.34
S50_1392   Diamond T620 Semi-Skirted Tanker         26             $105.33               $2,738.58
S24_1444   1970 Dodge Coronet                       21              $47.40                 $995.40
S24_2300   1962 Volkswagen Microbus                 70             $109.90               $7,693.00
S24_2840   1958 Chevy Corvette Limited Edition      30              $32.88                 $986.40
S12_4473   1957 Chevy Pickup                        54             $100.73               $5,439.42
========================================================================================================================
Total number of products per order: 11
Grand Total:     $46,895.48
========================================================================================================================
========================================================================================================================


Order Number: 10413


========================================================================================================================
Code       Product Name                             Quantity       Price               Subtotal
========================================================================================================================
S18_4027   1970 Triumph Spitfire                    49             $133.57               $6,544.93
S32_3207   1950's Chicago Surface Lines Streetcar   24              $56.55               $1,357.20
S50_1514   1962 City of Detroit Streetcar           51              $53.31               $2,718.81
S12_1108   2001 Ferrari Enzo                        36             $201.57               $7,256.52
S12_3148   1969 Corvair Monza                       47             $145.04               $6,816.88
S12_3891   1969 Ford Falcon                         22             $173.02               $3,806.44
========================================================================================================================
Total number of products per order: 6
Grand Total:     $28,500.78
========================================================================================================================
========================================================================================================================


Order Number: 10414


========================================================================================================================
Code       Product Name                             Quantity       Price               Subtotal
========================================================================================================================
S18_3029   1999 Yamaha Speed Boat                   44              $77.42               $3,406.48
S18_3140   1903 Ford Model A                        41             $128.39               $5,263.99
S18_3259   Collectable Wooden Train                 48              $85.71               $4,114.08
S18_4522   1904 Buick Runabout                      56              $83.38               $4,669.28
S700_1138  The Schooner Bluenose                    37              $62.00               $2,294.00
S700_1938  The Mayflower                            34              $74.48               $2,532.32
S700_2610  The USS Constitution Ship                31              $61.44               $1,904.64
S700_3505  The Titanic                              28              $84.14               $2,355.92
S700_3962  The Queen Mary                           40              $84.41               $3,376.40
S72_3212   Pont Yacht                               47              $54.60               $2,566.20
S24_2011   18th century schooner                    43             $108.14               $4,650.02
S24_3151   1912 Ford Model T Delivery Wagon         60              $72.58               $4,354.80
S24_3816   1940 Ford Delivery Sedan                 51              $72.96               $3,720.96
S10_4757   1972 Alfa Romeo GTA                      49             $114.24               $5,597.76
========================================================================================================================
Total number of products per order: 14
Grand Total:     $50,806.85
========================================================================================================================
========================================================================================================================


Order Number: 10415


========================================================================================================================
Code       Product Name                             Quantity       Price               Subtotal
========================================================================================================================
S18_3856   1941 Chevrolet Special Deluxe Cabriolet  51              $86.81               $4,427.31
S700_2047  HMS Bounty                               32              $73.32               $2,346.24
S72_1253   Boeing X-32A JSF                         42              $43.20               $1,814.40
S24_2841   1900s Vintage Bi-Plane                   21              $60.97               $1,280.37
S24_3420   1937 Horch 930V Limousine                18              $59.83               $1,076.94
========================================================================================================================
Total number of products per order: 5
Grand Total:     $10,945.26
========================================================================================================================
========================================================================================================================


Order Number: 10416


========================================================================================================================
Code       Product Name                             Quantity       Price               Subtotal
========================================================================================================================
S18_2581   P-51-D Mustang                           15              $70.96               $1,064.40
S24_4278   1900s Vintage Tri-Plane                  48              $70.28               $3,373.44
S32_1374   1997 BMW F650 ST                         45              $86.90               $3,910.50
S32_4289   1928 Ford Phaeton Deluxe                 26              $68.10               $1,770.60
S50_1341   1930 Buick Marquette Phaeton             37              $39.71               $1,469.27
S700_1691  American Airlines: B767-300              23              $88.60               $2,037.80
S700_2466  America West Airlines B757-200           22              $84.76               $1,864.72
S700_2834  ATA: B757-300                            41              $98.48               $4,037.68
S700_3167  F/A 18 Hornet 1/72                       39              $65.60               $2,558.40
S700_4002  American Airlines: MD-11S                43              $63.67               $2,737.81
S24_1785   1928 British Royal Navy Airplane         47              $90.82               $4,268.54
S24_2000   1960 BSA Gold Star DBD34                 32              $62.46               $1,998.72
S24_3949   Corsair F4U ( Bird Cage)                 18              $64.83               $1,166.94
S18_1662   1980s Black Hawk Helicopter              24             $129.31               $3,103.44
========================================================================================================================
Total number of products per order: 14
Grand Total:     $35,362.26
========================================================================================================================
========================================================================================================================


Order Number: 10417


========================================================================================================================
Code       Product Name                             Quantity       Price               Subtotal
========================================================================================================================
S18_2625   1936 Harley Davidson El Knucklehead      36              $58.75               $2,115.00
S24_1578   1997 BMW R 1100 S                        35             $109.32               $3,826.20
S10_1678   1969 Harley Davidson Ultimate Chopper    66              $79.43               $5,242.38
S10_2016   1996 Moto Guzzi 1100i                    45             $116.56               $5,245.20
S10_4698   2003 Harley-Davidson Eagle Drag Bike     56             $162.67               $9,109.52
S12_2823   2002 Suzuki XREO                         21             $144.60               $3,036.60
========================================================================================================================
Total number of products per order: 6
Grand Total:     $28,574.90
========================================================================================================================
========================================================================================================================


Order Number: 10418


========================================================================================================================
Code       Product Name                             Quantity       Price               Subtotal
========================================================================================================================
S18_3278   1969 Dodge Super Bee                     16              $70.76               $1,132.16
S18_3482   1976 Ford Gran Torino                    27             $139.64               $3,770.28
S18_3782   1957 Vespa GS150                         33              $56.57               $1,866.81
S18_4721   1957 Corvette Convertible                28             $120.53               $3,374.84
S24_4620   1961 Chevrolet Impala                    10              $66.29                 $662.90
S32_2206   1982 Ducati 996 R                        43              $36.61               $1,574.23
S32_4485   1974 Ducati 350 Mk3 Desmo                50             $100.01               $5,000.50
S50_4713   2002 Yamaha YZR M1                       40              $72.41               $2,896.40
S24_2360   1982 Ducati 900 Monster                  52              $64.41               $3,349.32
========================================================================================================================
Total number of products per order: 9
Grand Total:     $23,627.44
========================================================================================================================
========================================================================================================================


Order Number: 10419


========================================================================================================================
Code       Product Name                             Quantity       Price               Subtotal
========================================================================================================================
S18_1984   1995 Honda Civic                         34             $133.72               $4,546.48
S18_2870   1999 Indy 500 Monte Carlo SS             55             $116.16               $6,388.80
S18_3232   1992 Ferrari 360 Spider red              35             $165.95               $5,808.25
S18_3685   1948 Porsche Type 356 Roadster           43             $114.44               $4,920.92
S24_2972   1982 Lamborghini Diablo                  15              $32.10                 $481.50
S24_3371   1971 Alpine Renault 1600s                55              $52.66               $2,896.30
S24_3856   1956 Porsche 356A Coupe                  70             $112.34               $7,863.80
S12_1099   1968 Ford Mustang                        12             $182.90               $2,194.80
S12_3380   1968 Dodge Charger                       10             $111.57               $1,115.70
S12_3990   1970 Plymouth Hemi Cuda                  34              $64.64               $2,197.76
S12_4675   1969 Dodge Charger                       32              $99.04               $3,169.28
S18_1129   1993 Mazda RX-7                          38             $117.48               $4,464.24
S18_1589   1965 Aston Martin DB5                    37             $100.80               $3,729.60
S18_1889   1948 Porsche 356-A Roadster              39              $67.76               $2,642.64
========================================================================================================================
Total number of products per order: 14
Grand Total:     $52,420.07
========================================================================================================================
========================================================================================================================


Order Number: 10420


========================================================================================================================
Code       Product Name                             Quantity       Price               Subtotal
========================================================================================================================
S18_2248   1911 Ford Town Car                       36              $52.06               $1,874.16
S18_2325   1932 Model A Ford J-Coupe                45             $116.96               $5,263.20
S18_4409   1932 Alfa Romeo 8C2300 Spider Sport      66              $73.62               $4,858.92
S18_4933   1957 Ford Thunderbird                    36              $68.42               $2,463.12
S24_1046   1970 Chevy Chevelle SS 454               60              $60.26               $3,615.60
S24_1628   1966 Shelby Cobra 427 S/C                37              $48.80               $1,805.60
S24_1937   1939 Chevrolet Deluxe Coupe              45              $32.19               $1,448.55
S24_2766   1949 Jaguar XK 120                       39              $76.33               $2,976.87
S24_2887   1952 Citroen-15CV                        55             $115.09               $6,329.95
S24_3191   1969 Chevrolet Camaro Z28                35              $77.05               $2,696.75
S24_3432   2002 Chevy Corvette                      26             $104.94               $2,728.44
S24_3969   1936 Mercedes Benz 500k Roadster         15              $35.29                 $529.35
S18_1749   1917 Grand Touring Sedan                 37             $153.00               $5,661.00
========================================================================================================================
Total number of products per order: 13
Grand Total:     $42,251.51
========================================================================================================================
========================================================================================================================


Order Number: 10421


========================================================================================================================
Code       Product Name                             Quantity       Price               Subtotal
========================================================================================================================
S18_2795   1928 Mercedes-Benz SSK                   35             $167.06               $5,847.10
S24_2022   1938 Cadillac V-16 Presidential Limousin 40              $44.80               $1,792.00
========================================================================================================================
Total number of products per order: 2
Grand Total:      $7,639.10
========================================================================================================================
========================================================================================================================


Order Number: 10422


========================================================================================================================
Code       Product Name                             Quantity       Price               Subtotal
========================================================================================================================
S18_1342   1937 Lincoln Berline                     51              $91.44               $4,663.44
S18_1367   1936 Mercedes-Benz 500K Special Roadster 25              $47.44               $1,186.00
========================================================================================================================
Total number of products per order: 2
Grand Total:      $5,849.44
========================================================================================================================
========================================================================================================================


Order Number: 10423


========================================================================================================================
Code       Product Name                             Quantity       Price               Subtotal
========================================================================================================================
S18_2949   1913 Ford Model T Speedster              10              $89.15                 $891.50
S18_2957   1934 Ford V8 Coupe                       31              $56.21               $1,742.51
S18_3136   18th Century Vintage Horse Carriage      21              $98.44               $2,067.24
S18_3320   1917 Maxwell Touring Car                 21              $80.36               $1,687.56
S24_4258   1936 Chrysler Airflow                    28              $78.89               $2,208.92
========================================================================================================================
Total number of products per order: 5
Grand Total:      $8,597.73
========================================================================================================================
========================================================================================================================


Order Number: 10424


========================================================================================================================
Code       Product Name                             Quantity       Price               Subtotal
========================================================================================================================
S18_4668   1939 Cadillac Limousine                  26              $40.25               $1,046.50
S32_3522   1996 Peterbilt 379 Stake Bed with Outrig 44              $54.94               $2,417.36
S700_2824  1982 Camaro Z28                          46              $85.98               $3,955.08
S10_1949   1952 Alpine Renault 1300                 50             $201.44              $10,072.00
S12_1666   1958 Setra Bus                           49             $121.64               $5,960.36
S18_1097   1940 Ford Pickup Truck                   54             $108.50               $5,859.00
========================================================================================================================
Total number of products per order: 6
Grand Total:     $29,310.30
========================================================================================================================
========================================================================================================================


Order Number: 10425


========================================================================================================================
Code       Product Name                             Quantity       Price               Subtotal
========================================================================================================================
S18_2238   1998 Chrysler Plymouth Prowler           28             $147.36               $4,126.08
S18_2319   1964 Mercedes Tour Bus                   38             $117.82               $4,477.16
S18_2432   1926 Ford Fire Engine                    19              $48.62                 $923.78
S18_3232   1992 Ferrari 360 Spider red              28             $140.55               $3,935.40
S18_4600   1940s Ford truck                         38             $107.76               $4,094.88
S32_1268   1980Æs GM Manhattan Express              41              $83.79               $3,435.39
S32_2509   1954 Greyhound Scenicruiser              11              $50.32                 $553.52
S50_1392   Diamond T620 Semi-Skirted Tanker         18              $94.92               $1,708.56
S24_1444   1970 Dodge Coronet                       55              $53.75               $2,956.25
S24_2300   1962 Volkswagen Microbus                 49             $127.79               $6,261.71
S24_2840   1958 Chevy Corvette Limited Edition      31              $31.82                 $986.42
S10_4962   1962 LanciaA Delta 16V                   38             $131.49               $4,996.62
S12_4473   1957 Chevy Pickup                        33              $95.99               $3,167.67
========================================================================================================================
Total number of products per order: 13
Grand Total:     $41,623.44
========================================================================================================================


Total number of orders: 15
Total Value:    $441,474.94
======================================================End Of File=======================================================

PL/SQL procedure successfully completed.
*/