SELECT MAX(OrderNumber) FROM Orders;

/*
MAX(ORDERNUMBER)
----------------
           10425 (last order number)
*/

desc orders;

/*
 Name                                                              Null?    Type
 ----------------------------------------------------------------- -------- --------------------------------------------
 ORDERNUMBER                                                       NOT NULL NUMBER(11)
 ORDERDATE                                                         NOT NULL DATE
 REQUIREDDATE                                                      NOT NULL DATE
 SHIPPEDDATE                                                                DATE
 STATUS                                                            NOT NULL VARCHAR2(15)
 COMMENTS                                                                   VARCHAR2(500)
 CUSTOMERNUMBER                                                    NOT NULL NUMBER(11)
*/

DROP SEQUENCE orderNum_seq;
CREATE SEQUENCE orderNum_seq
MINVALUE 10426
MAXVALUE 99999999999
START WITH 10426
INCREMENT BY 1
NOCACHE;

select sysdate from dual;

/*
SYSDATE
---------
27-MAR-23
*/

INSERT INTO Orders
VALUES(orderNum_seq.nextval, '27-MAR-23', '31-MAR-23', '28-MAR-23', 'In Progress', NULL, 496);

select * from customers1;

/*
CUSTID CUSTNAME                       CUSTCREDITLIMIT CUSTCREDITAMOUNT
------ ------------------------------ --------------- ----------------
C00001 Eric Heng                                 1500              636
C00002 Alice Ooi                                 1000              636
*/

desc customers1;

/*
 Name                                                              Null?    Type
 ----------------------------------------------------------------- -------- --------------------------------------------
 CUSTID                                                            NOT NULL CHAR(6)
 CUSTNAME                                                          NOT NULL VARCHAR2(30)
 CUSTCREDITLIMIT                                                            NUMBER(6,2)
 CUSTCREDITAMOUNT                                                           NUMBER(6,2)
*/

DROP SEQUENCE custNum_seq;
CREATE SEQUENCE custNum_seq
MINVALUE 3
MAXVALUE 99999
START WITH 3
INCREMENT BY 1
NOCACHE;

INSERT INTO Customers1
VALUES ('C' || TO_CHAR(custNum_seq.nextval, 'FM00000'), 'Han Yu', 2000, 0);

SELECT * FROM customers1;

/*
CUSTID CUSTNAME                       CUSTCREDITLIMIT CUSTCREDITAMOUNT
------ ------------------------------ --------------- ----------------
C00003 Han Yu                                    2000                0
C00004 Han Yu                                    2000                0
C00001 Eric Heng                                 1500              636
C00002 Alice Ooi                                 1000              636
*/