/*
Practical 3

CREATE OR REPLACE FUNCTION fun_getAge(v_dob IN DATE) RETURN NUMBER IS
BEGIN
 RETURN EXTRACT (YEAR FROM (SYSDATE - v_dob) YEAR TO MONTH);
END;
/
SELECT name FROM user_source;
SELECT text FROM user_source WHERE name = 'FUN_GETAGE';
*/

ALTER TABLE Employees
ADD (DOB DATE);

CREATE OR REPLACE TRIGGER trg_check_employee_age
BEFORE INSERT ON Employees
FOR EACH ROW
BEGIN
    IF (fun_getAge(:NEW.DOB) < 18) THEN
        RAISE_APPLICATION_ERROR (-20000, 'Age below 18 years olds.');
    END IF;
END;
/

ALTER SESSION SET NLS_DATE_FOPRMAT = 'YYYY-MM-DD';
INSERT INTO Employees(EmployeeNumber, FirstName, lastName, DOB)
VALUES (99999, 'Sharen', 'Tang', '2021-12-16');


--2nd method
CREATE OR REPLACE PROCEDURE prc_insert_employee
(v_empNo IN NUMBER, v_firstName IN VARCHAR, v_lastName IN VARCHAR, v_dov IN DATE) IS

TOO_YOUNG EXCEPTION;
PRAGMA EXCEPTION_INIT(TOO_YOUNG, -20000);
BEGIN
    IF (fun_getAge(v_dob) < 18) THEN
        RAISE TOO_YOUNG;
    ELSE
        INSERT INTO Employees (EmployeeNumber, FirstName, LastName, DOB)
        VALUES (v_empNo, v_firstName, v_lastName, v_dob);
    END IF;

EXCEPTION
    WHEN TOO_YOUNG THEN
        DBMS_OUTPUT.PUT_LINE('Age below 18 years olds.' || CHR(10) || 'Cannot add ' || v_firstName || ' ' || v_lastName);
END;
/

INSERT INTO Employees (EmployeeNumber, FirstName, LastName, DOB)
VALUES (99999, 'Sharen', 'Tang', '2021-12-16');